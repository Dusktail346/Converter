import subprocess



def __convert(f_path_in, f_path_out):
    s_path_out = f"{f_path_out[:f_path_out.rindex('.')]}.srt"

    subprocess.check_call([
        "bash",
        "./utils/extract-all-subtitles.sh",
        f_path_in
    ])

    subprocess.check_call([
        "ffmpeg",
        "-nostdin", "-hide_banner",
        "-i", f_path_in,
        "-map", "0",            # Map (select) all streams
        "-codec:v", "libx264",  # Convert video streams to AVC (x264)
        "-codec:a", "libopus",  # Convert audio streams to opus
        "-sn",                  # Don't copy subtitles
        "-codec:d", "copy",     # Copy additional data (attachment) (default)
        #"-b:v", "",            # Video bitrate
        #"-b:a", "48k",         # Audio bitrate
        "-crf", "23",
        f_path_out
    ])
