import subprocess



def __convert(f_path_in, f_path_out):
    subprocess.call([
        "pandoc",
        "-M", "title: ",
        "-s", f_path_in,
        "-o", f_path_out
    ])
