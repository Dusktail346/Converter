import subprocess



def __convert(f_path_in, f_path_out):
    subprocess.check_call([
        "ffmpeg",
        "-nostdin", "-hide_banner",
        "-i", f_path_in,
        "-map", "0",            # Map (select) all streams
        "-codec:v", "vp9",      # Convert video streams to vp9
        "-codec:a", "libopus",  # Convert audio streams to opus
        "-sn",                  # Don't copy subtitles
        "-dn",                  # Don't copy data (attachements)
        "-crf", "23",
        f_path_out
    ])
