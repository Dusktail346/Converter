import subprocess



def __convert(f_path_in, f_path_out):
    subprocess.check_call([
        "dwebp",
        f_path_in,
        "-o", f_path_out
    ])
